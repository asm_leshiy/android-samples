package com.example.leshiy.questionnaire;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "asm";

    EditText fullNameTxt;
    EditText ageTxt;
    SeekBar salarySlider;
    TextView salaryTxt;
    RadioButton question01RightAnswer;
    RadioButton question02RightAnswer;
    RadioButton question03RightAnswer;
    RadioButton question04RightAnswer;
    RadioButton question05RightAnswer;
    CheckBox question06CheckBox;
    CheckBox question07CheckBox;
    CheckBox question08CheckBox;
    Button sendButton;
    TextView resultText;

    int scores = 0;
    boolean nameIsValid = false;
    boolean ageIsValid = false;
    boolean salaryIsValid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fullNameTxt = findViewById(R.id.fio_input);
        ageTxt = findViewById(R.id.age_input);
        salarySlider = findViewById(R.id.salary_slider);
        salaryTxt = findViewById(R.id.salary_txt);
        question01RightAnswer = findViewById(R.id.q1_3_rightAnswer);
        question02RightAnswer = findViewById(R.id.q2_3_rightAnswer);
        question03RightAnswer = findViewById(R.id.q3_2_rightAnswer);
        question04RightAnswer = findViewById(R.id.q4_1_rightAnswer);
        question05RightAnswer = findViewById(R.id.q5_1_rightAnswer);
        question06CheckBox = findViewById(R.id.q6_checkBox);
        question07CheckBox = findViewById(R.id.q7_checkBox);
        question08CheckBox = findViewById(R.id.q8_checkBox);
        sendButton = findViewById(R.id.send_button);
        resultText = findViewById(R.id.resultText);

        fullNameTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String str = ((EditText) v).getText().toString().trim();
                    fullNameTxt.setText(str);

                    if(fullNameValid(str))
                    {
                        fullNameTxt.setTextColor(Color.GRAY);
                        nameIsValid = true;
                    }
                    else
                    {
                        fullNameTxt.setTextColor(Color.RED);
                        nameIsValid = false;
                    }
                }
            }
        });

        ageTxt.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                String str = ((EditText) v).getText().toString().trim();
                ageTxt.setText(str);

                if(str.matches("[0-9]+"))
                {
                    ageTxt.setTextColor(Color.GRAY);
                    ageIsValid = true;
                }
                else
                {
                    ageTxt.setTextColor(Color.RED);
                    ageIsValid = false;
                }
            }
        });

        salarySlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                salaryTxt.setText((String.valueOf(progress) + " $"));
                salaryIsValid = progress >= 800 && progress <= 1600;
            }
        });
    }

    boolean fullNameValid(String str) {
        str = str.trim();
        String temp = str.replaceAll("\\s+","");
        if(!temp.matches("[a-zA-Z]+"))
        {
            Log.i(TAG, "error : digits have founded : " + temp);
            return false;
        }

        String[] array = str.split(" ");
        if(array.length >= 2)
        {
            for(String s : array)
                if(s.length() <= 2)
                {
                    Log.i(TAG, "substring : " + s);
                    return false;
                }

            return true;
        }

        Log.i(TAG, "error : fullNameTxt not valid");
        return  false;
    }

    public void onSendBtnClick(View view) {
        if(!nameIsValid){
            Toast.makeText(this, R.string.nameNotValid, Toast.LENGTH_SHORT).show();
            return;
        }

        if(!ageIsValid){
            Toast.makeText(this, R.string.ageNotValid, Toast.LENGTH_SHORT).show();
            return;
        }

        int age = Integer.parseInt(ageTxt.getText().toString());
        if(age < 21 || age > 40 ){
            if(age < 21) ShowResult(false, getResources().getString(R.string.youAreToYoung));
            if(age > 40) ShowResult(false, getResources().getString(R.string.youAreToOld));
            return;
        }

        if(!salaryIsValid){
            ShowResult(false, getResources().getString(R.string.salaryNotValid));
            return;
        }

        if(question01RightAnswer.isChecked()) scores += 2;
        if(question02RightAnswer.isChecked()) scores += 2;
        if(question03RightAnswer.isChecked()) scores += 2;
        if(question04RightAnswer.isChecked()) scores += 2;
        if(question05RightAnswer.isChecked()) scores += 2;
        if(question06CheckBox.isChecked()) scores += 2;
        if(question07CheckBox.isChecked()) scores += 1;
        if(question08CheckBox.isChecked()) scores += 1;

        if(scores >= 10)
            ShowResult(true, getResources().getString(R.string.callUs));
        else
            ShowResult(false, getResources().getString(R.string.empty));

        Log.i(TAG, "SCORES = " + scores);
    }

    void ShowResult(boolean success, String description){
        sendButton.setVisibility(Button.GONE);
        if (success) {
            resultText.setText(String.format("%1$s \n\n %2$s", getResources().getString(R.string.goodResult), description));
            resultText.setTextColor(Color.GRAY);
        }
        else {
            resultText.setText(String.format("%1$s \n\n %2$s", getResources().getString(R.string.badResult), description));
            resultText.setTextColor(Color.RED);
        }
        resultText.setVisibility(TextView.VISIBLE);
    }
}
