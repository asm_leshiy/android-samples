package com.example.leshiy.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "asm";

    TextView msg;
    Operand firstArgument, secondArgument;
    String operator = "";

    // ================================================================= //

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        msg = findViewById(R.id.msg);
        reset();
    }

    // ================================================================= //

    void setArgumentValue(char ch)
    {
        if(operator == "")
            firstArgument.AddSymbol(ch);
        else
            secondArgument.AddSymbol(ch);

        showMessageText();
    }

    void showMessageText()
    {
        String output = String.format("%1$s %2$s %3$s", firstArgument.getValue(), operator, secondArgument.getValue());
        Log.i(TAG, output);
        msg.setText(output);
    }

    void doOperation(String operation)
    {
        if(operator != "")
        {
            Log.i(TAG, "operator = " + operator);
            operation = operator == "" ? operation : operator;

            switch(operation)
            {
                case "+":
                    firstArgument.setValue(firstArgument.asDouble() + secondArgument.asDouble());
                    msg.setText(firstArgument.getValue());
                    break;
                case "-":
                    firstArgument.setValue(firstArgument.asDouble() - secondArgument.asDouble());
                    msg.setText(firstArgument.getValue());
                    break;
                case "*":
                    firstArgument.setValue(firstArgument.asDouble() * secondArgument.asDouble());
                    msg.setText(firstArgument.getValue());
                    break;
                case "/":
                    if(secondArgument.asDouble() != 0)
                    {
                        firstArgument.setValue(firstArgument.asDouble() / secondArgument.asDouble());
                        msg.setText(firstArgument.getValue());
                    }
                    else
                    {
                        reset();
                        msg.setText("Divide by zero error");
                    }

                    break;
            }

            secondArgument = new Operand();
            operator = "";
        }
        else
        {
            operator = operation;
            String output = String.format("%1$s %2$s %3$s", firstArgument.getValue(), operator, secondArgument.getValue());
            Log.i(TAG, output);
            msg.setText(output);
        }
    }

    void reset()
    {
        firstArgument = new Operand();
        firstArgument.setValue("0");
        secondArgument = new Operand();
        operator = "";
        msg.setText(firstArgument.getValue());
    }

    // ================================================================= //

    public void onResetBtnClick(View view)
    {
        reset();
    }

    public void onPlusBtnClick(View view)
    {
        if(secondArgument.isExist())
            doOperation("+");
        else
        {
            operator = "+";
            showMessageText();
        }
    }

    public void onMinusBtnClick(View view)
    {
        if(operator == "")
        {
            if(firstArgument.getValue() == "0" || firstArgument.getValue() == "0.0")
            {
                if(!firstArgument.isNegative())
                    firstArgument.AddSymbol('-');
            }
            else
            {
                if(firstArgument.isValidValue())
                    operator = "-";
            }
            showMessageText();
        }
        else
        {
            if(secondArgument.isExist())
                doOperation("-");
            else
            {
                operator = "-";
                showMessageText();
            }
        }
    }

    public void onMultiplyBtnClick(View view)
    {
        if(secondArgument.isExist())
            doOperation("*");
        else
        {
            operator = "*";
            showMessageText();
        }
    }

    public void onDivideBtnClick(View view)
    {
        if(secondArgument.isExist())
            doOperation("/");
        else
        {
            operator = "/";
            showMessageText();
        }
    }

    public void onDotBtnClick(View view)
    {
        if(operator == "")
            firstArgument.AddSymbol('.');
        else
            secondArgument.AddSymbol('.');

        showMessageText();
    }

    public void onBackspaceBtnClick(View view)
    {
        if(operator == "")
            firstArgument.removeLastSymbol();
        else
            secondArgument.removeLastSymbol();

        showMessageText();
    }

    public void onEqualsBtnClick(View view)
    {
        doOperation(operator);
    }

    // ================================================================= //

    public void onZeroBtnClick(View view)
    {
        setArgumentValue('0');
    }

    public void onOneBtnClick(View view)
    {
        setArgumentValue('1');
    }

    public void onTwoBtnClick(View view)
    {
        setArgumentValue('2');
    }

    public void onThreeBtnClick(View view)
    {
        setArgumentValue('3');
    }

    public void onFourBtnClick(View view)
    {
        setArgumentValue('4');
    }

    public void onFiveBtnClick(View view)
    {
        setArgumentValue('5');
    }

    public void onSixBtnClick(View view)
    {
        setArgumentValue('6');
    }

    public void onSevenBtnClick(View view)
    {
        setArgumentValue('7');
    }

    public void onEightBtnClick(View view)
    {
        setArgumentValue('8');
    }

    public void onNineBtnClick(View view)
    {
        setArgumentValue('9');
    }

    // ================================================================= //

}

class Operand
{
    private static final String TAG = "asm";

    String value;

    public Operand()
    {
        value = "";
    }

    public boolean isExist()
    {
        return value != null && value != "";
    }

    public boolean isDouble()
    {
        return isExist() && value.contains(".");
    }

    public boolean isNegative()
    {
        return isExist() && value.contains("-");
    }

    public double asDouble()
    {
        return Double.parseDouble(value);
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setValue(double value)
    {
        this.value = Double.toString(value);
    }

    public void AddSymbol(char ch)
    {
        if(!isExist())
            value = "0";

        if(ch == '.')
        {
            if(!isDouble())
            {
                if(isNegative() && value.length() == 1)
                    value = "-0" + ch;
                else
                    value += ch;
            }
            return;
        }
        else if(ch == '-')
        {
            if(!isNegative())
            {
                if(value == "0")
                    value = new String(new char[] { ch });
            }
            return;
        }

        if(value == "0")
        {
            if(ch != '0')
            {
                value = new String(new char[] { ch });
                return;
            }
        }
        else
        {
            value += ch;
            return;
        }
    }

    public void removeLastSymbol()
    {
        if(!isExist())
            return;
        else if(value.length() == 1)
        {
            value = "";
            return;
        }

        char[] origin = value.toCharArray();
        char[] chars = new char[origin.length - 1];
        for (int i = 0; i < chars.length; i++)
            chars[i] = origin[i];
        value = new String(chars);
    }

    public boolean isValidValue()
    {
        try
        {
            Double.parseDouble(value);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public int length()
    {
        return value.length();
    }
}