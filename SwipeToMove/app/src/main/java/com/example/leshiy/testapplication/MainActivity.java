package com.example.leshiy.testapplication;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "LOG";

    ConstraintLayout container;
    ImageView img;
    TextView msg;
    float[] x = {0f, 0f};
    float[] y = {0f, 0f};
    float screenWidth = 0f;
    float screenHeight = 0f;
    boolean isMoving = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = findViewById(R.id.background);
        img = findViewById(R.id.img);
        msg = findViewById(R.id.msg_txt);

        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                screenWidth = container.getWidth();
                screenHeight = container.getHeight();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        Log.i(TAG, "onTouchEvent: " + event);

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                x[0] = event.getX();
                y[0] = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                x[1] = event.getX();
                y[1] = event.getY();
                moveImage();
                break;
        }

        return super.onTouchEvent(event);
    }

    private void moveImage()
    {
        float dx = x[0] - x[1];
        float dy = y[0] - y[1];

        int imgWidth = img.getWidth();
        int imgHeight = img.getHeight();

        if (Math.abs(dy) > Math.abs(dx))
        {
            if (dy > 0)
            {
                if(img.getY() - imgHeight > 0 && !isMoving)
                {
                    msg.setText("Move Up");
                    MoveTo(img, 0, imgHeight - screenHeight, 1000);
                }
            }
            else
            {
                if(img.getY() + imgHeight < screenHeight && !isMoving)
                {
                    msg.setText("Move Down");
                    MoveTo(img, 0, screenHeight - imgHeight, 1000);
                }
            }
        }
        else
        {
            if (dx > 0)
            {
                if(img.getX() - imgWidth > 0 && !isMoving)
                {
                    msg.setText("Move Left");
                    MoveTo(img, imgWidth - screenWidth, 0, 1000);
                }
            }
            else
            {
                if(img.getX() + imgWidth < screenWidth && !isMoving)
                {
                    msg.setText("Move Right");
                    MoveTo(img, screenWidth - imgWidth, 0, 1000);
                }
            }
        }
    }

    void MoveTo(ImageView img, float posX, float posY, long time)
    {
        if(isMoving) return;
        else isMoving = true;

        img.animate()
            .translationXBy(posX)
            .translationYBy(posY)
            .setDuration(time)
            .withEndAction(new Runnable()
            {
                @Override
                public void run()
                {
                    isMoving = false;
                }
            }).start();
    }
}
